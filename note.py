from workart import Workart

class Note(object):

    def __init__(self, title, workart, episode, text = "No text"):
        self._title = title
        self._workart = workart
        self._episode = episode
        self._text = text

   
    @property
    def title(self):
        return self._title

    
    @property
    def text(self):
        return self._text


    def edit(self, text):
        self._text = text


