from workart import Workart
from exceptions import WorkartAlreadyExists, WorkartNotFoundError,\
                       EmptyNameException

class Category(object):

    def __init__(self, name):
        self._name = name
        self._workarts = []
    

    @property
    def name(self):
        return self._name


    def change_name(self, new_name):
        if (new_name.strip() == ''):
            raise EmptyNameException('Category needs name')
        self._name = new_name


    @property
    def workarts(self):
        return self._workarts


    def add_workart(self, workart):
        if workart in self._workarts:
            raise WorkartAlreadyExists('Workart already exists.')
        self._workarts.append(workart)


    def rm_workart(self, name):
        for w in self._workarts:
            if w.name == name:
                self._workarts.remove(w)
                return
        raise WorkartNotFoundError(\
                'Does not exists a workart with name \"' + name + '\".')


    def find(self, workart_name):
        for w in self._workarts:
            if w.name == workart_name:
                return w
        return None


    def __eq__(self, other):
        return self._name == other.name


    def __ne__(self, other):
        return self._name != other.name
