from w_state import WorkartState

class Workart(object):
    "Class that represents an workart (anime, book or series).\
            It works like an abstract class, do not instantiate it."

    def __init__(self, name, authors, description = "No description",\
            episodes = 1, current = 1):
        self._name = name
        self._authors = authors
        self._description = description
        self._episodes = episodes
        self._current = current
        self._state = WorkartState.IN_PROGRESS
   

    @property
    def name(self):
        return self._name


    @property
    def authors(self):
        return self.__authors


    def description(self):
        return self._description
   

    def set_description(self, description):
        self._description = description


    @property
    def episodes(self):
        return self._episodes


    def current(self):
        return self._current


    def set_current(self, current):
        self._current = current


    current = property(current, set_current)
    description = property(description, set_description)



class Anime(Workart):

    def __init__(self, name, authors, description = "No description",
            episodes = 1, current_episode = 1):
        super().__init__(name, authors, description, episodes, current_episode)



class Book(Workart):

    def __init__(self, name, authors, description = "No description", pages = 1,
            current_page = 1):
        super().__init__(name, authors, description, pages, current_page)



class Series(Workart):

    def __init__(self, name, authors, description, seasons = 1,\
            current_season = 1, current_episode = 1):
        super().__init__(name, authors, description, current_episode)
        self._seasons = [None]*seasons
        self._current_season = current_season
    

    def current_season(self):
        return self._current_season


    def set_current_season(season):
        self._current_season = season


    def def_season_episodes(self, season, episodes):
        self._seasons[season-1] = episodes


    @property  # override
    def episodes(self):
        return sum(self._seasons)


    current_season = property(current_season, set_current_season)    
