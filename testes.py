from anime import Anime
from book import Book
from note import Note
from series import Series
from workart    import Workart
from category   import Category
from exceptions import *
from user import User

a = Anime("obra 1", "autor obra 1", "No description", 305, 10)
b = Anime("obra 2", "autor obra 2", "No description", 800, 800)
c = Book("obra 3", "autor obra 3", "No description", 500, 50)
d = Book("obra 4", "autor obra 4", "No description", 700, 700)
e = Series("obra 5, autor obra 5", "No description", 10, 9, 15)
f = Series("obra 6", "autor obra 6", "No description", 5, 10, 15)
f.def_season_episodes(1 , 10)
f.def_season_episodes(2 , 9)
f.def_season_episodes(3 , 10)
f.def_season_episodes(4 , 12)
f.def_season_episodes(5 , 11)

u = User("123")

# Criar categoria
try:
    print("Adicionando categoria1")
    u.create_category("categoria personalizada1")
    print("Adicionando categoria1 NOVAMENTE")
    u.create_category("categoria personalizada1")
except CategoryDoesNotExistsException as e:
    print("ERR Categoria não existe: ", e.message)
except CategoryAlreadyExistsException as e:
    print("ERR Categoria já existe ", e.message)

try: 
    print("Adicionando categoria2")
    u.create_category("categoria personalizada2")
except CategoryDoesNotExistsException as e:
    print("ERR Categoria não existe: ", e.message)
except CategoryAlreadyExistsException as e:
    print("ERR Categoria já existe ", e.message)

try:
    print("Adicionando obra a em cp1")
    u.add_workart(a, "categoria personalizada1")
    print("Adicionando obra b em others")
    u.add_workart(b)
    print("Adicionando obra a NOVAMENTE em cp1")
    u.add_workart(a, "categoria personalizada1")
except CategoryDoesNotExistsException:
    print("Categoria não existe")
except WorkartNotFoundError as e:
    print("Obra não encontrada em categoria ", e.message)
except WorkartAlreadyExists as e:
    print("Obra já existe em categoria ", e.message)

try: 
    print("Adicionando obra a uma categoria nao criada")
    u.add_workart(a, "naocriada")
except CategoryDoesNotExistsException as e:
    print("Categoria nao criada", e.message)

try:
    print("Mover obras")
    u.mv_workart("obra 1", "categoria personalizada1", "categoria personalizada2")
except CategoryDoesNotExistsException as e:
    print("Categoria nao existe", e.message)

try:
    print("Deletando categorias")
    u.rm_category("categoria personalizada2")
except CategoryDoesNotExistsException as e:
    print("Categoria nao existe", e.message)

try:
    u.add_workart(f, "others")
    u.add_workart(f, "categoria personalizada1")
    print("Criando uma nota")
    u.schedule_episode("Quero ver essa", "obra 6", 16, "Lembrar de chamar a helli")
    u.schedule_episode("Quero ver essa2", "obra 1", 10, "Lembrar de chamar a helli")
    print(u._notes)
except WorkartNotFoundError as e:
    print("Obra não encontrada", e.message)

try:
    print("Removendo nota2")
    u.rm_note("Quero ver essa2")
    print(u._notes)
except TitleNotFoundError as e:
    print("Titulo da obra não encontrado", e.message)
