from workart import *

class DataBase(object):
    
    _instance = None

    def __init__(self):
        if DataBase._instance != None:
            return
        self.fill()
        DataBase._instance = self


    @classmethod
    def get_instance(self):
        if DataBase._instance == None:
            DataBase()
        return DataBase._instance


    @property
    def workarts(self):
        return tuple(self.__workarts)


    def get(self, workart_name):
        for workart in self.__workarts:
            if workart.name == workart_name:
                return workart
        return None

    
    def add_workart(self, workart):
        if workart not in self.__workarts:
            self.__workarts.append(workart)


    def fill(self):
        a = '''In 1988 the Japanese government drops an atomic bomb on Tokyo after ESP experiments on children go awry. In 2019, 31 years after the nuking of the city, Kaneda, a bike gang leader, tries to save his friend Tetsuo from a secret government project. He battles anti-government activists, greedy politicians, irresponsible scientists and a powerful military leader until Tetsuo's supernatural powers suddenly manifest. A final battle is fought in Tokyo Olympiad exposing the experiment's secrets.'''
        c = '''In 1972, Misaki, a popular student of Yomiyama North Middle School's class 3-3, suddenly died partway through the school year. Devastated by the loss, the students and teacher behaved like Misaki was still alive, leading to a strange presence on the graduation photo. In Spring 1998, Kōichi Sakakibara transfers into Yomiyama's class 3-3, where he meets Mei Misaki, a quiet student whom their classmates and teacher seemingly ignore. The class is soon caught up in a strange phenomenon, in which students and their relatives begin to die in often gruesome ways. Realizing that these deaths are related to the "Misaki of 1972", a yearly calamity that has struck most every class 3-3 since 1972, Kōichi and Mei seek to figure out how to stop it before it kills anymore of their classmates or them.'''

        workart1 = Anime("akira.jpg", "Katsuhiro Otomo", a)
        workart2 = Anime("dragon-ball-Z.jpg", "Akira Toriyama")
        workart3 = Anime("another.jpg", "Yakito Ayatsuji", c)

        b = '''In a time long forgotten, a preternatural event threw the seasons off balance. In a land where summers can last decades and winters a lifetime, trouble is brewing. As the cold returns, sinister forces are massing beyond the protective wall of the kingdom of Winterfell. To the south, the king's powers are failing, with his most trusted advisor mysteriously dead and enemies emerging from the throne's shadow. At the center of the conflict, the Starks of Winterfell hold the key: a reluctant Lord Eddard is summoned to serve as the king's new Hand, an appointment that threatens to sunder both family and kingdom. In this land of extremes, plots and counterplots, soldiers and sorcerers, each side fights to win the deadliest of conflicts: the game of thrones.'''


        descricao1 = '''A high school student discovers a supernatural notebook that has deadly powers. He can kill anyone he wishes simply by inscribing their name within its pages. Intoxicated with his new power, he begins to eliminate those he deems unworthy of life.'''
        workart6 = Anime("death-note.jpg", "Tsugumi Ohba", descricao1, 30, 23)

        descricao2 = '''A história apresenta criaturas chamadas Mushi (蟲? lit. Inseto) que frequentemente apresentam poderes sobrenaturais. Os mushis são descritos como seres em contato com a essência da vida na sua forma mais básica e pura. Devido à sua natureza efêmera, a maioria dos seres humanos é incapaz de perceber os mushis e é alheia à sua existência, mas há alguns que possuem a capacidade de ver e interagir com tais seres. Uma dessas pessoas é Ginko (ギンコ?), o personagem principal da série. Ele emprega-se como um "Mushishi" (termo dado a pessoas que estudam os mushis e desenvolvem formas de lidar com os mesmos), viajando de um lugar para outro a pesquisar mushis e ajudar pessoas que sofrem de problemas causados por eles. A série é uma antologia de episódios em que os únicos elementos em comum entre os episódios são Ginko e os vários tipos de mushi. Não há excesso de arqueamento da trama.'''
        workart5 = Anime("mushishi.jpg", "Desconhecido", descricao2)


        workart7 = Book("game-of-thrones.jpg", "David Benioff", b)
        workart8 = Book("o-pequeno-principe.jpg", "Antoine de Saint-Exupéry")
        workart9 = Anime("hunter-x-hunter.jpg", "Yoshihiro Togashi")
        workart10 = Anime("full-metal-alchemist:brotherhood.jpg", "Hiromu Arakawa")
        self.__workarts = [workart1, workart2, workart3, workart5, workart6, workart7, workart8, workart9, workart10]


