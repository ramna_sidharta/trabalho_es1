from note        import Note
from category    import Category
from exceptions  import WrongPasswordException, CategoryAlreadyExistsException,\
                        CategoryDoesNotExistsException, WorkartNotFoundError,\
                        TitleNotFoundError

class User(object):

    _instance = None
    
    def __init__(self):
        if User._instance != None:
            return
        self._password = ""
        self._categories = {"others": Category("others")}
        self._notes = {} 
        User._instance = self


    @classmethod
    def get_instance(self):
        if User._instance == None:
            User()
        return User._instance

    
    @property
    def categories(self):
        return list(self._categories.values())


    def set_pw(self, password):
        if self._password == "":
            self._password = password
        else:
            ...
            #raise PasswordAlreadyDefinedException


    def verify_pw(self, password):
        return self._password == password
   
    
    def change_pw(self, curr_pw, new_pw):
        if curr_pw != self._password:
            raise WrongPasswordException
        self._password = new_pw


    def create_category(self, name):
        if name in self._categories:
            raise CategoryAlreadyExistsException
        self._categories[name] = Category(name)
    

    def rm_category(self, name):
        try:
            if(name != "others"):
                for w in self._categories[name].workarts:
                    self.mv_workart(w.name, name, "others")
                del(self._categories[name])
        except KeyError:
            raise CategoryDoesNotExistsException


    def add_workart(self, workart, ct_name = "others"):
        try:
            self._categories[ct_name].add_workart(workart)
        except KeyError:
            raise CategoryDoesNotExistsException


    def add_workart_multiple(self, w_name, ctgs):
        for c in ctgs:
            self.add_workart(w_name, c)


    def rm_workart(self, w_name, ct_name):
        try:
            self._categories[ct_name].rm_workart(w_name)
        except KeyError:
            raise CategoryDoesNotExistsException


    def rm_workart_multiple(self, w_name, ctgs):
        for c in ctgs:
            self.rm_workart(w_name, c)


    def mv_workart(self, w_name, cto_name, ctd_name):
        try:
            w = self._categories[cto_name].find(w_name)
            self._categories[cto_name].rm_workart(w_name)
            self.add_workart(w, ctd_name)
        except KeyError:
            raise CategoryDoesNotExistsException


    def add_workart_BD(self):
        ...


    def get_all_workarts(self):
        set_return = {}
        for c in self._categories:
            for w in self._categories[c].workarts:
                set_return[w.name] = w
        return set_return


    def schedule_episode(self, title, w_name, w_episode, description):
        set_workarts = self.get_all_workarts()
        try:
            note = Note(title, set_workarts[w_name], w_episode, description)
            self._notes[title] = note
        except KeyError:
            raise WorkartNotFoundError

    
    def edit_note(self, title, new_text):
        try:
            self._notes[title].edit(new_text)
        except KeyError:
            raise TitleNotFoundError


    def rm_note(self, title):
        try:
            del(self._notes[title])
        except KeyError:
            raise TitleNotFoundError
