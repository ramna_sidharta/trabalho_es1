from enum import Enum

class WorkartState(Enum):
    IN_PROGRESS = 1
    INTEREST    = 2
    FINISHED    = 3
    STOPPED     = 0

