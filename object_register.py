import os
import pickle

class ObjectRegister(object):

    def __init__(self, relative_path_file):
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        self.__file = os.path.join(absolute_path, relative_path_file)


    def load(self, load_option):
        with open(self.__file, load_option) as file:
            unpickler = pickle.Unpickler(file)
            return unpickler.load()


    def store(self, to_store, write_option):
        with open(self.__file, write_option) as file:
            pickle.dump(to_store, file)
