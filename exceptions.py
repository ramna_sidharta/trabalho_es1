class WorkartAlreadyExists(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)



class WorkartNotFoundError(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)
    
class TitleNotFoundError(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)
    

class WrongPasswordException(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)



class CategoryAlreadyExistsException(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)



class CategoryDoesNotExistsException(Exception):

    def __init__(self, message = ''):
        self.message = message


    def __str__(self):
        return repr(self.message)



class EmptyNameException(Exception):

    def __init__(self, message = ''):
        self.messege = message


    def __str__(self):
        return repr(self.message)

