#!/usr/bin/env python

import sys

from window          import Window
from workart_widget  import WorkartWidget
from PyQt5.QtWidgets import QApplication


def main():
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    app.exec_()

main()
