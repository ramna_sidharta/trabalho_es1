from user import User

# all get function refeers to an user workart (only workarts added by user)

def get_workart(w_name):
    user = User.get_instance()
    categories = user.categories
    workart = None
    for c in categories:
        w = c.find(w_name)
        if w: 
            return w
    return None


def get_episodes(w_name):
    w = get_workart(w_name)
    if w:
        return w.episodes
    return -1


def get_current_ep(w_name):
    w = get_workart(w_name)
    if w:
        return w.current
    return -1


def percentage_consumed(w_name):
    eps = get_episodes(w_name)
    curr = get_current_ep(w_name)
    return curr/eps*100


def consumed_content_str(w_name):
    eps = get_episodes(w_name)
    curr = get_current_ep(w_name)
    return (str(curr) + '/' + str(eps))

