import sys, os, os.path

from user import User

from PyQt5          import QtCore, QtGui, QtWidgets
from w_frame        import WorkartFrame
from w_button       import WorkartButton
from qt_home_widget import Ui_Form


class HomeWidget(Ui_Form, QtWidgets.QWidget):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setupUi(self)
        self.scrollAreasContents = [ self.scrollAreaWidgetContents,
                                     self.scrollAreaWidgetContents_2,
                                     self.scrollAreaWidgetContents_3 ]
        self.scrollAreasContents2 = [ self.scrollAreaWidgetContents_4,
                                      self.scrollAreaWidgetContents_5,
                                      self.scrollAreaWidgetContents_6 ]
        self.gridLayouts = [ self.gridLayout,
                             self.gridLayout_2,
                             self.gridLayout_3 ]
        self.gridLayouts2 = [ self.gridLayout_4,
                             self.gridLayout_5,
                             self.gridLayout_6 ]
        self.w_types = ('animes', 'series', 'books')  # will be tabs 1, 2 and 3
        self.workarts_tabs_names()
        self.set_frames()


    def workarts_tabs_names(self):
        for i in range(0, len(self.scrollAreasContents)):
            self.workartsTabs.setTabText(i, self.w_types[i])

        for i in range(0, len(self.scrollAreasContents2)):
            self.workartsTabs_2.setTabText(i, self.w_types[i])


    def set_frames(self):
        scroll_area = self.scrollAreaWidgetContents

        imgs_dir = "/home/sidharta/git/trabalho_es1/view/images/"
        directory = os.fsencode(imgs_dir)
        tab = 0
        for dir in self.w_types:
            path = os.fsencode(imgs_dir + dir)
            for file in os.listdir(path):
                fn = os.fsencode(file)
                if fn.endswith(b".jpg"):
                    img_name = file.decode("utf-8")
                    self.new_frame(img_name, self.scrollAreasContents, self.gridLayouts, tab)
            tab += 1

        self.set_frames_user()


    def set_frames_user(self):
        scroll_area = self.scrollAreaWidgetContents_2
        categories = User.get_instance().categories

        for c in categories:
            for w in c.workarts:
                if type(w) == "Anime":
                   self.new_frame(w.name, self.scrollAreasContents2,\
                           self.gridLayouts2, 0)
                elif type(w) == "Series":
                   self.new_frame(w.name, self.scrollAreasContents2,\
                           self.gridLayouts2, 1)
                else:
                   self.new_frame(w.name, self.scrollAreasContents2,\
                           self.gridLayouts2, 0)


    def new_frame(self, img, scrollAreasContents, gridLayouts, tab):
        frame = WorkartFrame(scrollAreasContents[tab])
        frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        flayout = QtWidgets.QVBoxLayout(frame)

        button = self.new_button(frame, img, tab)
        label = self.new_label(frame, img)
        frame.set_button(button)
        frame.set_label(label)
        flayout.addWidget(button)
        flayout.addWidget(label)

        gridLayouts[tab].setColumnStretch(0, 0)
        gridLayouts[tab].setColumnStretch(1, 0)
        gridLayouts[tab].setColumnStretch(2, 0)
        gridLayouts[tab].setColumnStretch(3, 0)
        gridLayouts[tab].addWidget(frame)


    def new_button(self, frame, img_name, tab):
        img_path = self._build_img_path(img_name, tab)
        btn = WorkartButton(frame)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(img_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        btn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        btn.setIcon(icon)
        btn.setIconSize(QtCore.QSize(100, 200))
        btn.setFlat(True)
        return btn


    def new_label(self, frame, img):
        lbl = QtWidgets.QLabel(frame)
        font = QtGui.QFont("Sans Serif", 11)
        lbl.setFont(font)
        lbl.setAlignment(QtCore.Qt.AlignCenter)
        lbl.setText(img)
        lbl.setWordWrap(True)
        return lbl

    
    def _build_img_path(self, img_name, tab):
        proj_path = os.environ['PYTHONPATH'].split(os.pathsep)
        return proj_path[0] + "view/images/" + self.w_types[tab] + '/' + img_name    
