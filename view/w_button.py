from PyQt5 import QtWidgets, QtGui
from workart_widget import WorkartWidget

class WorkartButton(QtWidgets.QPushButton):

    def __init__(self, parent):
        super().__init__(parent)
        self._parent = parent
        self.clicked.connect(self.action)


    def action(self):
        # import here for reasons of circular dependency
        from window import Window

        window = Window.get_instance()
        frame = self._parent
        wdg = WorkartWidget(frame)
        wdg.update_progress()
        window.set_top_widget(wdg)

