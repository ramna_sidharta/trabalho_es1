from qt_window import Ui_MainWindow
from PyQt5.QtWidgets import QMainWindow, QApplication

class Window(Ui_MainWindow, QMainWindow):

    _instance = None

    def __init__(self):
        if Window._instance != None:
            return
        QMainWindow.__init__(self)
        self.setWindowTitle("Ctrl + S.A.B")
        self.setupUi(self)
        self._current_stack_index = 0
        self.config_menubar()
        Window._instance = self


    @classmethod
    def get_instance(self):
        if Window._instance == None:
            Window()
        return Window._instance

    
    def config_menubar(self):
        self.actionBack.setShortcut("Ctrl+B")
        self.actionQuit.setShortcut("Ctrl+Q")
        self.set_actions()


    def set_top_widget(self, widget):
        self._current_stack_index += 1
        self.stackedWidget.addWidget(widget) 
        self.stackedWidget.setCurrentIndex(self._current_stack_index)

    
    def set_actions(self):
        self.actionBack.triggered.connect(self.back)
        self.actionQuit.triggered.connect(self.quit)


    def back(self):
        if self._current_stack_index == 0:
            return
        self._current_stack_index -= 1
        curr_wg = self.stackedWidget.currentWidget()
        self.stackedWidget.removeWidget(curr_wg)
        self.stackedWidget.setCurrentIndex(self._current_stack_index)


    def quit(self):
        exit(0)
