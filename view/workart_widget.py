import info_provider as infos

from user            import User
from data_base       import DataBase
from qt_workart      import Ui_Form
from add_window      import AddWindow

from PyQt5           import QtGui, QtCore
from PyQt5.QtWidgets import QWidget, QPushButton, QMessageBox


class WorkartWidget(Ui_Form, QWidget):

    def __init__(self, frame):
        QWidget.__init__(self)
        self.setupUi(self)
        self.frame = frame
        self.rm_button = QPushButton("Remove")
        self.verticalLayout_2.addWidget(self.rm_button)
        self.rm_button.clicked.connect(self._rmbtn_action)
        self.add_button.clicked.connect(self._addbtn_action)
        self.title = frame.label.text()
        self._set_content()

    
    def rst_options_buttons(self):
        if self.in_user_list():
            self.add_button.setEnabled(False)
            self.rm_button.setEnabled(True)
            self.ctg_comboBox.clear()

            # for each category of workart <self.title> -> comboBox.addItem().
            user_ctgs = User.get_instance().categories
            for c in user_ctgs:
                if c.find(self.title):  # if workart is in category c then
                    self.ctg_comboBox.addItem(c.name)
        else:
            self.add_button.setEnabled(True)
            self.rm_button.setEnabled(False)
            self.ctg_comboBox.clear()
            self.set_progress_text()


    def set_progress_text(self):
        if self.in_user_list():
            self.progress_label.setText(\
                    "Progress: " + infos.consumed_content_str(self.title))
        else:
            # eps deve ser o número de episódios da obra que está no BD
            self.progress_label.setText("Progress:")#0/" + str(eps))
            self.progressBar.setValue(0)


    def update_progress(self):
        w_name = self.title_groupBox.title()
        percentage = infos.percentage_consumed(w_name)
        self.progressBar.setValue(percentage)
        self.set_progress_text()

    
    def update_categories(self, ctgs):
        if ctgs:
            self.ctg_comboBox.addItems(ctgs)


    def in_user_list(self):
        return bool(infos.get_workart(self.title))


    def _rmbtn_action(self):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setText(\
                "Are you sure to remove workart from your category(ies)?")
        msg_box.setInformativeText(\
                "You will lost all modifications and informations of it\
                (notes, number of episodes, current episode, etc)")
        msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg_box.setDefaultButton(QMessageBox.Cancel)
        result = msg_box.exec_()
        if result == QMessageBox.Ok:
            self._remove()

        
    def _remove(self):
        user = User.get_instance()
        w_ctgs = []
        for c in user.categories:
            if c.find(self.title):
                w_ctgs.append(c.name)
        user.rm_workart_multiple(self.title, w_ctgs)
        self.rst_options_buttons()


    def _addbtn_action(self):
        from window import Window
        add_window = AddWindow(self, Window.get_instance())
        add_window.show()
        
        stacked_wdg = Window.get_instance().stackedWidget
        home_wdg = stacked_wdg.widget(stacked_wdg.currentIndex() - 1)
        num_tabs = home_wdg.workartsTabs_2.count()
        for t in range(0, num_tabs):
            t_name = home_wdg.workartsTabs_2.tabText(t)
            self_tab_name = self.left_groupBox.title()
            if t_name == self_tab_name:
                home_wdg.new_frame(self.title, home_wdg.scrollAreasContents2,\
                        home_wdg.gridLayouts2, t)



    def _set_content(self):
        # import here for reasons of circular dependency
        from window import Window

        window = Window.get_instance()
        home_widget = window.stackedWidget.currentWidget()
        w_tabs = home_widget.workartsTabs
        w_type = w_tabs.tabText(w_tabs.currentIndex())
        self._set_groupBox_titles(w_type)
        self._set_description()
        self._set_img(w_type)
        # quando houver banco de dados:
        # ver categorias default de obra e adicionar na comboBox

        self.rst_options_buttons()

    
    def _set_groupBox_titles(self, w_type):
        self.title_groupBox.setTitle(self.title)
        self.left_groupBox.setTitle(w_type)
    

    def _set_description(self):
        w_obj = DataBase.get_instance().get(self.title)
        self.textEdit.setReadOnly(True)
        self.textEdit.setText(w_obj.description)
        

    def _set_img(self, w_type):
        img_path = "images/" + w_type.lower() + '/' + self.title
        image = QtGui.QPixmap(img_path);
        self.image_label.setPixmap(image)
        self.image_label.setScaledContents(True)
        self.image_label.show()
