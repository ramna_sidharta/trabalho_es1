import info_provider as infos

from user      import User
from data_base import DataBase

from PyQt5          import QtWidgets, QtGui
from qt_add_workart import Ui_MainWindow

class AddWindow(Ui_MainWindow, QtWidgets.QMainWindow):
    
    def __init__(self, parent_wdg, parent_window = None):
        super(AddWindow, self).__init__(parent_window)
        self.setupUi(self)
        self.parent_wdg = parent_wdg
        self._ctgs_buttons = []
        self._set_categories()
        self.confirmButtonBox.accepted.connect(self._confirm)


    def _set_categories(self):
        user = User.get_instance()
        i = 1
        for c in user.categories:
            ctg_check_button = QtWidgets.QCheckBox(c.name)
            self._ctgs_buttons.append(ctg_check_button)
            self.verticalLayout.insertWidget(i, ctg_check_button)
            i += 1


    def _confirm(self):
        personal_text = self.personalTextEdit.toPlainText()
        curr_ep = self.spinBox.value()
        w_name = self.parent_wdg.title

        w = DataBase.get_instance().get(w_name)

        # avisar se curr_ep for maior do que o número atual de episódios da obra
        w.set_current(curr_ep)
        
        # names of selecteds categories
        ctgs = [c.text() for c in self._ctgs_buttons if c.isChecked()]
        User.get_instance().add_workart_multiple(w, ctgs)

        self.parent_wdg.update_progress()
        self.parent_wdg.update_categories(ctgs)
        self.parent_wdg.rst_options_buttons()
        self.close()
        
