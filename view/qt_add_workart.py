# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/add_workart.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(403, 306)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(-1, -1, 401, 281))
        self.widget.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.groupBox = QtWidgets.QGroupBox(self.widget)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ctgLabel = QtWidgets.QLabel(self.groupBox)
        self.ctgLabel.setObjectName("ctgLabel")
        self.verticalLayout.addWidget(self.ctgLabel)
        self.newCtgButton = QtWidgets.QPushButton(self.groupBox)
        self.newCtgButton.setObjectName("newCtgButton")
        self.verticalLayout.addWidget(self.newCtgButton)
        self.currEpLabel = QtWidgets.QLabel(self.groupBox)
        self.currEpLabel.setObjectName("currEpLabel")
        self.verticalLayout.addWidget(self.currEpLabel)
        self.spinBox = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox.setObjectName("spinBox")
        self.verticalLayout.addWidget(self.spinBox)
        self.gridLayout.addWidget(self.groupBox, 0, 0, 1, 1)
        self.personalTextEdit = QtWidgets.QTextEdit(self.widget)
        self.personalTextEdit.setObjectName("personalTextEdit")
        self.gridLayout.addWidget(self.personalTextEdit, 0, 1, 1, 1)
        self.confirmButtonBox = QtWidgets.QDialogButtonBox(self.widget)
        self.confirmButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.confirmButtonBox.setObjectName("confirmButtonBox")
        self.gridLayout.addWidget(self.confirmButtonBox, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 403, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.ctgLabel.setText(_translate("MainWindow", "Categories:"))
        self.newCtgButton.setText(_translate("MainWindow", "New Category"))
        self.currEpLabel.setText(_translate("MainWindow", "Current episode:"))
        self.personalTextEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Insert <span style=\" font-size:10pt;\">a personal description here</span></p></body></html>"))

